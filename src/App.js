import React from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import Navbar from './components/Layout/Navbar';
import Home from './components/Home';
import Signin from './components/Auth/Signin';
import Header from './components/Layout/Header';
import Footer from './components/Layout/Footer';
import NotFound from './components/NotFound/NotFound';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link, browserHistory
} from "react-router-dom";

import './App.css';
import Signup from './components/Auth/Signup';

function App() {
  return (

    <div className="App">

      <Router>
        <Navbar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/signin" component={Signin} />
          <Route path="/signup" component={Signup} />
          <Route component={NotFound} />
        </Switch>
      </Router>


    </div>
  );
}

export default App;
