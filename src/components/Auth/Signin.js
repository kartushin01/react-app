import React from "react";
import { useForm } from "react-hook-form";
import API from '../../utils/api';

export default function Signin() {

  const { register, handleSubmit, errors } = useForm(); // initialize the hook
  const onSubmit = (data) => {
    console.log(data);
    API.post("signin", data);
  };

  return (
    <div className="container">
      <div className="row d-flex justify-content-center">
        <div className="col-5">
          <form onSubmit={handleSubmit(onSubmit)} ClassName="container" style={{ marginTop: "50px" }} >

            <legend><h1>Sign in</h1> </legend>
            <div className="form-group">
              <label htmlFor="email">Email address</label>
              <input name="email"
                ref={register({ required: "email is requared", minLength: 8 })}
                className="form-control" id="firstname" />
              {errors.email && <p className="text-danger">{errors.email.message}</p>}
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                name="password"
                className="form-control"
                id="password"
                ref={register({ required: "password is requared", minLength: { value: 8, message: "minimum 8 characters " } })}
              />
              {errors.password && <p className="text-danger">{errors.password.message}</p>}

            </div>
            <input type="submit" className="btn btn-primary" value="SignIn" />
          </form>
        </div>
      </div>
    </div>
  )
}