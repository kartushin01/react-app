import React from "react";
import { Link } from "react-router-dom";

const NavBar = () => {
    return (

        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="#">Brand</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarText">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <Link to="/" className="nav-link">Home</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/signin" className="nav-link">signin</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/signup" className="nav-link">signup</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/oops" className="nav-link">404</Link>
                    </li>
                </ul>

            </div>
        </nav>


    );
};

export default NavBar;
